package com.accessibility.chromeDevTool;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.accessibility.utils.FileUtils;

class JsFactory {

    ApplicationProperties applicationProperties;

    private static JsFactory INSTANCE = null;
    private String accessibility_content = null;
    private String jquery_content = null;

    synchronized static JsFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new JsFactory();
            INSTANCE.load();
        }
        return INSTANCE;
    }

    private void load() {
    	try {
    		  jquery_content = FileUtils.readFile("E:\\ECLIPSE-WORKSPACE\\xavient-demo\\src\\main\\resources\\chromeDevToolScripts\\jquery.min.js");
    	      accessibility_content =FileUtils.readFile("E:\\ECLIPSE-WORKSPACE\\xavient-demo\\src\\main\\resources\\chromeDevToolScripts\\axs_testing.js");
    	} catch (IOException e) {
    		System.out.println("!!!!! Exception occurred while reading file " + e.getMessage());
    	}
      
    }

    String getAccessibility_content() {
        return accessibility_content;
    }

    String getJquery_content() {
        return jquery_content;
    }

}

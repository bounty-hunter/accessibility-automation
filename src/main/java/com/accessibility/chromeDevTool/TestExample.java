package com.accessibility.chromeDevTool;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.accessibility.utils.Drivers;
import com.accessibility.utils.ScreenShotGenerator;

public class TestExample {

	private WebDriver driver;
	private String browser = "chrome";
	
	@BeforeClass
	public void setUp() {
		driver = Drivers.getDriver(browser);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		if (browser.equalsIgnoreCase("firefox")) {
			driver.manage().window().setSize(new Dimension(1920, 1080));
		} else {
			driver.manage().window().maximize();
		}
		driver.navigate().to("https://www.spectrum.com");
	}
	
	@Test
	public void testAccessibilityWithChromeDevToolsExtension() {
			String methodName = "testAccessibilityWithChromeDevToolsExtension";
		 	AccessibilityScanner scanner = new AccessibilityScanner(driver);
	        Map<String, Object> audit_report = scanner.runAccessibilityAudit();
	        if (audit_report.containsKey("plain_report")) {
	           System.out.println(audit_report.get("plain_report").toString());
	        }
	        ScreenShotGenerator.generateScreenShot(driver, methodName);
}
}

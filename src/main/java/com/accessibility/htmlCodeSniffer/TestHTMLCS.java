package com.accessibility.htmlCodeSniffer;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.accessibility.utils.Drivers;

public class TestHTMLCS {

	private WebDriver driver;
	private String browser = "chrome";
	
	@BeforeClass
	public void setUp() {
		driver = Drivers.getDriver(browser);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		if (browser.equalsIgnoreCase("firefox")) {
			driver.manage().window().setSize(new Dimension(1920, 1080));
		} else {
			driver.manage().window().maximize();
		}
		driver.navigate().to("https://www.spectrum.net/login/");
		//driver.navigate().to("https://www.google.com");
	}
	
	@Test
	public void testAccessibilityHTMLCodeSniffer() {
		System.out.println(Base.runHtmlCS(driver));
	}
	
}

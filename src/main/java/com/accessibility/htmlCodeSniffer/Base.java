package com.accessibility.htmlCodeSniffer;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class Base {

	/** It will invoke HTMLCodeSniffer in the opened URL
	 *   Standards for testing case 'WCAG2A':
                    case 'WCAG2AA':
                    case 'WCAG2AAA':
                    case 'Section508':
	 * @param driver
	 * @return	result
	 */
	public static String runHtmlCS(WebDriver driver) {
		
		return  (String) ((JavascriptExecutor) driver).executeScript("javascript:(function() {var _p='//squizlabs.github.io/HTML_CodeSniffer/build/';var _i=function(s,cb) {var sc=document.createElement('script');sc.onload = function() {sc.onload = null;sc.onreadystatechange = null;cb.call(this);};sc.onreadystatechange = function(){if(/^(complete|loaded)$/.test(this.readyState) ===  true){sc.onreadystatechange = null;sc.onload();}};sc.src=s;if (document.head) {document.head.appendChild(sc);} else {document.getElementsByTagName('head')[0].appendChild(sc);}}; var options={path:_p};_i(_p+'HTMLCS.js',function(){HTMLCSAuditor.run('WCAG2AA',null,options);});})();");
		// return  (String) ((JavascriptExecutor) driver).executeScript("javascript:(function() {var _p='https://cd988454.ngrok.io/HTMLCS/';var _i=function(s,cb) {var sc=document.createElement('script');sc.onload = function() {sc.onload = null;sc.onreadystatechange = null;cb.call(this);};sc.onreadystatechange = function(){if(/^(complete|loaded)$/.test(this.readyState) ===  true){sc.onreadystatechange = null;sc.onload();}};sc.src=s;if (document.head) {document.head.appendChild(sc);} else {document.getElementsByTagName('head')[0].appendChild(sc);}}; var options={path:_p};_i(_p+'HTMLCS1.js',function(){HTMLCSAuditor.run('Section508',null,options);});})();");
	}
}

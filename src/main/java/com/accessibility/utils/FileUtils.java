package com.accessibility.utils;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileUtils {

	/**
	 * @param fileName location of file 
	 * @return file content as a java string
	 * @throws IOException
	 */
	public static String readFile(String fileName) throws IOException {
	    BufferedReader br = new BufferedReader(new FileReader(fileName));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        return sb.toString();
	    } finally {
	        br.close();
	    }
	}
}

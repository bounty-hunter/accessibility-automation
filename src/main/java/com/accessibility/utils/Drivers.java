package com.accessibility.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Drivers {
	
	public static final String CHROME = "chrome";
	public static final String FIREFOX = "firefox";
	public static final String EDGE = "edge";
	
	private static WebDriver getChromeDriver() {
		System.setProperty("webdriver.chrome.driver",
						   "D:\\ECLIPSE-WORKSPACE\\xavient-demo\\src\\main\\resources\\drivers\\chromedriver-2.35.exe");
		
		return new ChromeDriver();
	}

	private static WebDriver getFirefoxDriver() {
		System.setProperty("webdriver.gecko.driver",
						   "D:\\ECLIPSE-WORKSPACE\\xavient-demo\\src\\main\\resources\\drivers\\geckodriver-v0.19.1.exe");
		
		return new FirefoxDriver();
	}
	
	private static WebDriver getEdgeDriver() {
		
		System.setProperty("webdriver.edge.driver",
						   "D:\\ECLIPSE-WORKSPACE\\xavient-demo\\src\\main\\resources\\drivers\\MicrosoftWebDriver.exe");
		
		return new EdgeDriver();
	}

	/** It returns WebDriver for specific browsers.
	 * @param	 	browserName name of the browser 
	 * @return		i.e "chrome" will return chromedriver instance
	 */
	public static WebDriver getDriver(String browserName) {
		WebDriver driver = null;
		
		switch (browserName.toLowerCase()) {
		case CHROME:
			driver = getChromeDriver();
			break;
		case FIREFOX:
			driver = getFirefoxDriver();
			break;
		case EDGE:
			driver = getEdgeDriver();
			break;
		}

		return driver;
	}
}

package com.accessibility.utils;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.openqa.selenium.WebDriver;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class ScreenShotGenerator {

	private static final String PNG="PNG";
	private static final String USER_DIR="user.dir";
	public static void generateScreenShot(WebDriver driver, String testCaseName) {
		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(100))
										   .takeScreenshot(driver);

		try {
			ImageIO.write(screenshot.getImage(), PNG,
					new File(System.getProperty(USER_DIR) + "\\screenshots\\" + testCaseName + PNG));
		} catch (IOException e) {
			System.out.println("###########  Exception occured: " + e.getMessage());
		}
	}
}

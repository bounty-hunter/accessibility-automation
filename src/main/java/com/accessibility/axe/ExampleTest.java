package com.accessibility.axe;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.accessibility.htmlCodeSniffer.Base;
import com.accessibility.utils.Drivers;
import com.deque.axe.AXE;

public class ExampleTest {

	private static final URL scriptUrl = ExampleTest.class.getResource("/axeScript/axe.min.js");
	private WebDriver driver;
	private String browser = "chrome";
	
	@BeforeClass
	public void setUp() {
		driver = Drivers.getDriver(browser);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		if (browser.equalsIgnoreCase("firefox")) {
			driver.manage().window().setSize(new Dimension(1920, 1080));
		} else {
			driver.manage().window().maximize();
		}
		driver.navigate().to("https://www.spectrum.net/login/");
	}
	
	@Test
	public void testAccessibilityWithAxe() {
		JSONObject responseJSON = new AXE.Builder(driver, scriptUrl).analyze();
		JSONArray violations = responseJSON.getJSONArray("violations");
		System.out.println(violations);
		if (violations.length() == 0) {
			System.out.println("No violations found");
		} else {
			AXE.writeResults("testAccessibilityWithAxe", responseJSON);
		}
	}
}
